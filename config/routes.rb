Rails.application.routes.draw do
  root to: 'users#index'
  resources :users

  get :login, to: 'sessions#new'
  delete :logout, to: 'sessions#destroy'
  resources :sessions, only: :create

  use_doorkeeper

  namespace :api do
    resources :users, only: :index
  end
end
