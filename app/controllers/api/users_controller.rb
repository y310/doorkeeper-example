module Api
  class UsersController < ApplicationController
    before_action :doorkeeper_authorize!

    def index
      @users = User.all
      render json: @users
    end
  end
end
